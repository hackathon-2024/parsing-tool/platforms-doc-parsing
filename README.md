# TRA³ TRAnsparency TRAcing TRAck : Parsing and standardization tool

## Objective
Implement a *standardisation* (parsing) tool, in the programming language of your choice, that can be used to structure the information present in previously collected (see deliverable 1, data collection modules) web pages of API documentation based to facilitate future reuse. The tool must take as input the resources collected (in HTML and markdown format) and return a JSON file (that complies with the JSON schema located in `schema.json`) as illustrated below.

<img src="img/hackathon2024-livrable2-EN.png" alt="drawing" width="600"/>

The technical difficulty stems from the diversity of the possible structures and layouts of the API documentation depending on the platform or the service. Regardless of the initial page format, all the information must eventually be synthetized the same way in a JSON file with a predefined format.

This tool would ultimately allow the scientific community to create a structured database gathering all the information related to existing platforms API endpoints.

## Context
The Digital Services Act (DSA), whose provisions are due to come into full effect on 17/02/2024, will in
particular make data from online platforms more accessible. This will enable researchers to carry out
more in-depth studies than before on the possible systemic risks that digital platforms could cause.

This access to data can take a number of different forms: either through direct provision by platforms
within their web pages dedicated to their online transparency documentation (transparency reports in
particular), or through access to programming interfaces (APIs), or through the provision of certain data
by the European Commission (transparency database).

Rapid access to clear, structured documentation is vital if these resources are to be used effectively by
the research community. At present, however, such documentation is often scattered and non-
standardized.

Moreover, by its very nature, this documentation, like the services it describes, is likely to evolve. This
makes it necessary to develop a tool for periodically consulting, extracting, structuring and, if
necessary, updating it in relation to a previous iteration, while retaining a history of its changes.

The aim of this challenge, is to implemment a stand-
alone tool based on modules for collecting part of an API's documentation or information from a
platform's transparency center, then returning the results in a structured way that facilitates and
standardizes their use by researchers and regulators

This deliverable follows the first one (data collection modules) that allow an automated retrieval of online platforms transparency documentation.

## Contribution

To contribute, you must first fork the repository in your dedicated workspace and open a Gitlab issue indicating the URL of your fork whenever ready to be reviewed. You can still make changes to your code until the submission deadline (01/22/2024). Your code will then be reviewed and evaluated.

### Rules

Your tool must output valid JSON files that comply with the JSON schema defined in `schema.json`.

Your repository must include clear indications on how to install and run your tool. 

Sample inputs and expected outputs are provided and located in the folder `examples/`.

### Reproducibility

You can choose the technical solution you want.  
The only requirement is that **the solution must be 100% reproducible**.

For example, in the case of an ML approach, transparency regarding the data used (access, whole pre-processing, ...) is therefore required.

## Evaluation

Automated validation tests (see the `.gitlab-ci.yml` file) will ensure that your tool outputs a valid JSON. Any non trivial code that passes the validation tests (i.e. that outputs a JSON on the example with the expected format) will immediately award you **5,000** points.

Upon the end of the challenge (01/22/2024), all the implemented tools will be evaluated on additional sample inputs, including inputs from other platforms than the example platforms (Twitter and TikTok). Your score will be computed based on a distance between the generated JSON files and the expected JSON file. You may win up to **500,000** points based on the quality of your generated JSON files and the generazability of your tool. 

A leaderboard, tracking the participants total points (including both data collection modules and the standardization tool) is available here: https://hackathon.peren.fr/fr/leaderboard. 
