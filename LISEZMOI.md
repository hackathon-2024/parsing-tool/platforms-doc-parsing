# TRA³ TRAnsparency TRAcing TRAck : Outil de standardisation

## Objectif
Développer un outil de *standardisation* (parsing), dans le langage de votre choix, permettant de structurer les informations présentes dans les documentations des API des plateformes collectées (voir le livrable 1 du TRA³) selon un modèle prédéfini afin d’en faciliter leurs réutilisations futures. Sa conception prend en entrée les pages collectées (aux formats HTML et markdown) et renvoie un JSON structuré (selon un schéma prédéfini voir le fichier `schema.json`) comme illustré sur le schéma ci-dessous.

<img src="img/hackathon2024-livrable2.png" alt="drawing" width="600"/>

La difficulté technique provient de la diversité des formats des pages de documentation d'API selon les plateformes, qui doivent toutes être analysées parsées de la même manière. 

Cet outil permettrait _in fine_ d'obtenir une base de données structurée de tous les _endpoints_ existants des API des plateformes.


## Contexte
Le règlement sur les services numériques (RSN), dont l'entrée en application complète des dispositions
est prévue le 17/02/2024, va notamment rendre plus accessibles les données issues des plateformes en
ligne. Ceci permettra aux chercheurs de mener des études plus approfondies qu'auparavant sur les
éventuels risques systémiques que pourraient causer les plateformes numériques.

Cet accès aux données peut se matérialiser sous différentes formes : soit par une mise à disposition
directe par les plateformes au sein de leurs pages web dédiées à leur documentation de transparence
en ligne (rapport de transparence notamment), soit par un accès à des interfaces de programmation
(API), soit par une mise à disposition de certaines données par la Commission Européenne
(transparency database).

Un accès rapide à une documentation claire et structurée est primordial pour une utilisation effective
de ces ressources par la communauté de chercheurs. Or actuellement cette documentation est
souvent éparse et non standardisée.

De plus, par nature, cette documentation, de même que les services qu’elle décrit, est susceptible
d'évoluer. De fait, cela rend nécessaire le développement d'un outil permettant sa consultation
périodique, son extraction, son formatage structuré, et, le cas échéant, sa mise à jour par rapport à une
précédente itération tout en conservant un historique de ses évolutions.

L'objectif de ce challenge est de co-développer un outil
autonome permettra de structurer la documentation de transparence collectée facilitant et standardisant leur utilisation par les chercheurs et les régulateurs.

Ce livrable fait suite au 1er livrable (modules de collecte) permettant de collecter la documentation de transparence de plateformes en ligne.

## Contribuer

Pour contribuer, il est nécessaire de forker le dépôt dans votre espace de travail dédié (voir lien envoyé par mail) puis d'indiquer via une *issue* GitLab l'URL de votre outil. Vous pourrez par la suite effectuer des changements sur votre dépôt jusqu'à la clôture du hackathon (22/01/2024), date à laquelle votre code sera évalué. 


### Validation d'un outil

L'outil développé doit renvoyer un fichiers JSON contenant un dictionnaire conforme au format défini dans le fichier `schema.json`.

Des scripts d'installation et un README doivent par ailleurs expliquer clairement l'installation et la prise en main de l'outil. 

Des exemples des entrées fournies et des sorties attendues se trouvent dans le dossier `examples/`.

### Reproducibility

Vous pouvez choisir la solution technique que vous souhaitez à condition que le comportement de votre outil soit  **entièrement reproductible**.

Typiquement, si vous utilisez du ML, une transparence sur les données utilisées est attendue (accès, pré-processing, ...).

## Evaluation

Des tests de validation automatique de la conformité du JSON soumis seront effectués pour garantir que le fichier JSON correspond au schéma attendu. Tout code non trivial permettant de générer à partir des données d'exemple un JSON respectant le format attendu donnera lieu à **5 000** points.

Une fois le challenge terminé (le 22 janvier), les outils proposés par les participants seront évalués sur d'autres fichiers, y compris sur des fichiers correspondant à des plateformes autres que celles données en exemple (Twitter et TikTok) et un score de proximité entre les JSON retournés et les JSON attendus sera calculé. Les participants pourront alors se voir octroyer jusqu'à **500 000** points en fonction de la qualité et du caractère généralisable de leur outil de standardisation.

Le leaderboard est partagé avec celui du premier livrable et est disponible à cette adresse : https://hackathon.peren.fr/fr/leaderboard. 
